<?php

namespace app\modules\jcms\admin\components;

use Yii;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\base\Widget;

class Crud extends \yii\base\Widget
{
    public $right;
    public $modelClass;
    public $onPage;
    public $listFields;
    public $filterFields;
    public $modelId;
    public $createCaption;
    public $listCaption;
    public $editCaption;
    public $defaultValues;
    public $afterInsert;
    public $save;
    public $allowCreate;
    public $allowSort;
    public $url;
    public $urlParams;
    public $backToListTitle;
    public $backToListLink;
    public $filterList;
    public $createButtonParams;
    public $urlAfterCreate;
    public $getModelId;
    public $getModelById;

    public function init() {
        parent::init();
    }

    public static function create($config)
    {
        $config['class'] = get_called_class();
        $component = Yii::createObject($config);
        return $component->run();
    }

    public function run()
    {
        $params = Yii::$app->controller->actionParams;
        $id = !empty($params['id']) ? $params['id'] : 0;
        $id = !empty($this->modelId) ? $this->modelId : $id;
        if ($id) {
            // Загружаем модель по $id
            if(!empty($this->getModelById)) {
                $model = call_user_func_array($this->getModelById, [$id]);
            }
            else {
                $model = call_user_func_array(array($this->modelClass, 'findOne'), array(array('id' => $id)));
            }
            if (!$model) {
                // 404
                $result = array('html' => '');
                $result['error'] = 'Модель не найдена.';
                return $result;
            }
        } else {
            // Создаем новую модель с данными по умолчанию
            $model = new $this->modelClass;
            if (!empty($this->defaultValues)) {
                foreach ($this->defaultValues as $key => $value) {
                    $model->{$key} = $value;
                }
            }
        }
        
        if($this->urlParams) {
            $this->urlParams = '?'.$this->urlParams;
        }
        
        switch (Yii::$app->controller->action->id) {
            case 'create':
                return $this->actionForm($model, true);
            case 'edit':
                return $this->actionForm($model);
            case 'index':
                return $this->displayList($model);
        }
        $result = ['html' => ''];
        $result['error'] = 'Нет обработчика для данного действия.';
        return $result;
    }

    /**
     * Отображение и сохранение формы
     * @param  \app\base\BaseModel
     * @param  boolean
     * @return array
     */
    private function actionForm($model, $isCreate = false)
    {
        $result = [];
        $errors = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($this->save) {
                    call_user_func_array($this->save, array(&$model, $isCreate));
                } else {
                    $model->save();
                }
                $result['reload_left_column'] = 1;
                if ($isCreate) {
                    if($this->urlAfterCreate) {
                        $result['redirect'] = str_replace('#id#', $model->id, $this->urlAfterCreate);
                    }
                    else {
                        $result['redirect'] = Yii::$app->controller->id.'/index'.$this->urlParams;
                    }
                    if ($this->afterInsert) {
                        call_user_func_array($this->afterInsert, array($model));
                    }
                }
            }
            else {
                $errors = $model->getErrors();
            }
        }
        $form = $model->getForm();
        if($this->backToListTitle && $this->backToListLink) {
            $form->addButton($this->backToListTitle, $this->backToListLink);
        }
        $form->setErrors($errors);

        $result['title'] = $isCreate ? $this->createCaption : $this->editCaption;
        $result['html'] = $form->run();

        return $result;
    }

    /**
     * Отображение списка моделей
     * @param  \app\base\BaseModel
     * @return array
     */
    private function displayList($model)
    {
        $class = get_class($model);
        $query = call_user_func(array($class, 'find'));
        $pageId = \Yii::$app->request->get('page_id');

        if ($pageId) {
            $query->andWhere(['page_id' => $pageId]);
        }

        // Обработка фильтров
        $this->applyFiltersIfNeeded($model, $query);

        // Добавляем кнопки по умолчанию, если мы не в режиме сортировки
        $sortMode = \Yii::$app->request->get('sortMode') == 1 ? true : false;
        if(!$sortMode) {
            $this->getDefaultButtons($model);
        }

        // Форма фильтров
        $filters = $this->getFiltersFormHtml($model);

        $result = [
            'html' => $this->render('@app/modules/jcms/admin/widgets/gridview/views/index.php', [
                'dataProvider' => $this->createDataProvider($query),
                'columns' => $this->listFields,
                'filters' => $filters,
                'onPage' => $this->onPage,
                'sortMode' => $sortMode,
                'modelId' => $this->modelId,
                'createButtonParams' => $this->createButtonParams,
                'allowCreate' => $this->allowCreate,
                'allowSort' => $this->allowSort,
                'url' => $this->url,
                'pageId' => $pageId,
            ]),
            'title' => $this->listCaption];
        return $result;
    }

    /**
     * Добавляет кнопки по умолчанию в список моделей
     * @param  \app\base\BaseModel
     * @return void
     */
    private function getDefaultButtons($model)
    {
        $this->listFields[] = [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons' => [
                'update'=>function ($url, $model) {
                    $id = !empty($this->getModelId) ? call_user_func_array($this->getModelId, [$model]) : $model->id;
                    $customUrl = $this->url.'/edit/'.$id.$this->urlParams;
                    return \yii\helpers\Html::a( '<i class="material-icons">create</i>', $customUrl, ['title' => 'Редактировать', 'class' => 'link_content link_edit', 'data-pjax' => '0']);
                },
                'delete'=>function ($url, $model) {
                    $id = !empty($this->getModelId) ? call_user_func_array($this->getModelId, [$model]) : $model->id;
                    $customUrl = $this->url.'/remove/'.$id.$this->urlParams;
                    return \yii\helpers\Html::a( '<i class="material-icons">delete</i>', $customUrl, ['title' => 'Удалить', 'class' => 'link_content', 'data-confirm-jcms' => 'Вы действительно хотите удалить элемент?', 'data-pjax' => '0']);
                }
            ]
        ];
    }

    /**
     * Применение фильтров если они заполнены
     * @param  \app\base\BaseModel
     * @param  \yii\db\Query
     * @return void
     */
    private function applyFiltersIfNeeded($model, & $query)
    {
        $class = get_class($model);
        $filter = $this->getFilterFromUrl($class);
        if($filter) {
            $form = $model->getForm();
            foreach($filter as $key => $value) {
                $fieldType = $this->getFieldType($key, $form, $model);
                if($fieldType == 'integer') {
                    $query->andWhere([$key => $value]);
                }
                elseif($fieldType == 'date') {
                    $exp = explode('-', $value);
                    if(count($exp) == 2) {
                        $query->andWhere(['>=', $key, date('Y-m-d 00:00:00', strtotime($exp[0]))]);
                        $query->andWhere(['<=', $key, date('Y-m-d 23:59:59', strtotime($exp[1]))]);
                    }
                }
                else {
                    $value = str_replace(' ', '%', $value);
                    $query->andWhere(['LIKE', $key, new \yii\db\Expression("'%".$value."%'")]);
                }
            }
        }
        
        // Дополнительная ползьвательский фильтр списка
        if(!empty($this->filterList)) {
            call_user_func_array($this->filterList, array(&$query));
        }
    }

    /**
     * @param  \yii\db\Query
     * @return \yii\data\ActiveDataProvider
     */
    private function createDataProvider($query)
    {
        $sortMode = \Yii::$app->request->get('sortMode') == 1 ? true : false;
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => $this->onPage, 
            ]
        ]);
        if($sortMode) {
            $dataProvider->setSort([
                'attributes' => ['sort'],
                'defaultOrder' => ['sort' => SORT_ASC],
            ]);
        }
        return $dataProvider;
    }

    /**
     * Возвращает тип поля в зависимости от правил валидации и класса
     * @param  string
     * @param  \app\forms\BaseForm
     * @param  \app\base\BaseModel
     * @return string
     */
    private function getFieldType($fieldName, & $form, & $model)
    {
        $class = $form->getFieldClass($fieldName);
        $exp = explode('\\', $class);
        $class = array_pop($exp);
        if(strpos($class, 'Date') !== false) {
            return 'date';
        }
        $rules = $model->rules();
        foreach($rules as $rule) {
            if(is_array($rule[0]) && in_array($fieldName, $rule[0]) && $rule[1] == 'integer') {
                return 'integer';
            }
        }
        return 'string';
    }

    /**
     * Возвращает массив фильтров из URL
     * @param string $class Класс модели
     * @return array
     */
    private function getFilterFromUrl($class)
    {
        $filter = array();
        $model = $this->getFilterModelFromUrl($class);
        if($model) {
            foreach($this->filterFields as $field) {
                if(!empty($model->$field)) {
                    $filter[$field] = $model->$field;
                }
            }
        }
        return $filter;
    }

    /**
     * Возвращает модель класса $class с заполенными полями из фильтра url
     * @param type $class 
     * @return mixed
     */
    private function getFilterModelFromUrl($class)
    {
        $flt = \Yii::$app->request->get('flt');
        if($flt && $fltUrl = json_decode($flt, true)) {
            $modelData = array();
            foreach($fltUrl as $key => $value) {
                if(preg_match('#([A-Za-z0-9]+)\[([a-zA-Z0-9]+)\]#Umsi', $key, $match)) {
                    if(!isset($modelData[$match[1]])) {
                        $modelData[$match[1]] = array();
                    }
                    $modelData[$match[1]][$match[2]] = $value;
                }
            }
            $model = new $class;
            $model->load($modelData);
            return $model;
        }
        return false;
    }

    /**
     * Возвращает html формы фильтров или false если фильтры не заданы
     * @param  \app\base\BaseModel
     * @return mixed
     */
    private function getFiltersFormHtml($model)
    {
        $filters = false;
        if($this->filterFields) {
            // Получаем виджет формы
            $formFilter = $model->getForm();
            $formFilter->setFilterFields($this->filterFields);
            $class = get_class($model);
            $filterModel = $this->getFilterModelFromUrl($class);
            if(!$filterModel) {
                $filterModel = new $class;
            }
            $formFilter->setModel($filterModel);
            $filters = $formFilter->run();
        }
        return $filters;
    }
}
