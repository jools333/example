<?php

namespace app\controllers;

use Yii;

use app\components\Basket;
use app\models\Zakaz;
use app\models\ZakazItem;
use app\modules\jcms\common\models\CommonSettings;
use app\forms\SettingsForm;

class BasketController extends BaseController 
{
    public function getViewPath()
    {
        return Yii::getAlias('@app/views/default/'.$this->id);
    }

    public function beforeAction($action)
    {            
        if ($action->id == 'add' || $action->id == 'update' || $action->id == 'send') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Отображение корзины
     * @param  \app\models\Page
     * @return string
     */
    public function actionIndex($page)
    {
        $this->page = $page;

        \Yii::$app->view->params['body_class'] = 'page_content basket_list p-0';

        $caption = $this->showMeta();

        $basketInfo = Basket::getBasketInfo();
        $percentToOpt = $this->getPercentToOptPrice($basketInfo['summa']);
        $summaToOpt = $this->getSummaToOptPrice($basketInfo['summa']);

        return $this->render('index', [
            'basket' => Basket::getBasket(),
            'info' => $basketInfo,
            'priceField' => $summaToOpt == 0 ? 'price_opt' : 'price',
            'percent' => $percentToOpt,
            'summaToOpt' => $summaToOpt,
            'caption' => $caption,
        ]);
    }

    /**
     * Возвращает оставшуюся сумму необходимую для получения оптовой цены
     * @param  float 
     * @return float
     */
    private function getSummaToOptPrice($basketSumma)
    {
        $optSumma = Basket::getOptSumma();
        $summaToOpt = $optSumma - $basketSumma;
        if($summaToOpt < 0) {
            $summaToOpt = 0;
        }
        return $summaToOpt;
    }

    /**
     * Возвращает оставшийся процент от суммы корзины для получения оптовой цены
     * @param  float 
     * @return integer
     */
    private function getPercentToOptPrice($basketSumma)
    {
        $optSumma = Basket::getOptSumma();
        $percentToOpt = ceil(($basketSumma*100)/$optSumma);
        if($percentToOpt > 100) {
            $percentToOpt = 100;
        }
        return $percentToOpt;
    }

    /**
     * Добавление товара в корзину
     * @return string
     */
    public function actionAdd()
    {
        $result = ['error' => '', 'basket' => '', 'isOpt' => false];

        $items = \Yii::$app->request->post('items');
        if($items) {
            foreach($items as $item) {
                $basketItem = Basket::add($item['id'], $item['color'], $item['size'], $item['num']);
                if($basketItem->getErrors()) {
                    $result['error'] = 'Ошибка добавления товара в корзину';
                }
            }
            $result['basket'] = Basket::getBasketInfo();
        }
        else {
            $result['error'] = 'Не указаны товары для добавления в корзину.';
        }

        return json_encode($result);
    }

    /**
     * Обновление количества товаров в корзине
     * @return string
     */
    public function actionUpdate()
    {
        $result = ['error' => '', 'basket' => '', 'isOpt' => false];

        $items = \Yii::$app->request->post('items');
        Basket::clear();
        if($items) {
            foreach($items as $item) {
                $basketItem = Basket::add($item['id'], $item['color'], $item['size'], $item['num']);
                if($basketItem->getErrors()) {
                    $result['error'] = 'Ошибка обновления товара в корзине';
                }
            }
            $result['basket'] = Basket::getBasketInfo();
        }
        return json_encode($result);
    }

    /**
     * Отправка корзины (заказа)
     * @return string
     */
    public function actionSend()
    {
        $result = ['error' => ''];
        $model = new Zakaz;
        $errors = [];
        $info = Basket::getBasketInfo();
        if(!$info['num']) {
            $errors[] = 'Корзина пуста'; 
        }

        if (!$errors && $model->load(Yii::$app->request->post()) && $model->validate()) {
            if(empty($model->phone) && empty($model->email)) {
                $errors[] = 'Необходимо указать E-mail или телефон';
            }
            else {
                $errors = array_merge($errors, $this->createZakazFromBasket($model));
            }
        }
        else {
            $errors = array_merge($errors, $model->getErrorsList());
        }

        if($errors) {
            $result['error'] = implode("\r\n", $errors);
        }

        return json_encode($result);
    }

    /**
     * @param  Zakaz
     * @return array
     */
    private function createZakazFromBasket($model)
    {
        $model->date_add = date('Y-m-d H:i:s');
        if(\Yii::$app->user->identity) {
            $model->user_id = \Yii::$app->user->identity->id;
        }
        if($model->save()) {
            $this->addZakazItems($model);
            $model->sendEmail();
            Basket::clear();
        }
        else {
            return array_merge($errors, $model->getErrorsList());
        }
        return [];
    }

    /**
     * Добавление в заказ позиций из корзины
     * @param Zakaz
     */
    private function addZakazItems($model)
    {
        $info = Basket::getBasketInfo();
        $basket = Basket::getBasket(); 
        $priceField = $info['isOpt'] ? 'price_opt' : 'price';
        foreach($basket as $item) {
            $zakazItem = new ZakazItem;
            $zakazItem->zakaz_id = $model->id;
            $zakazItem->page_id = $item->page_id;
            $zakazItem->size_id = $item->size_id;
            $zakazItem->color_id = $item->color_id;
            $zakazItem->num = $item->num;
            $zakazItem->price = $item->page->settings->{$priceField};
            $zakazItem->save();
        }
    }
}
